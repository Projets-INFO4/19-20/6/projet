import pycom

from network import Bluetooth

import binascii
import ubinascii

from network import LoRa

import socket
import struct

import time

# ===== Functions =====

def color(ired: int, igreen: int, iblue:int) -> int:
    ired = ired & 0xff
    igreen = igreen & 0xff
    iblue = iblue & 0xff
    return (ired << 8*2) + (igreen << 8) + iblue

def uuid2bytes(uuid):
    uuid = uuid.encode().replace(b'-',b'')
    tmp = binascii.unhexlify(uuid)
    return bytes(reversed(tmp))


# ===== Init =====

pycom.heartbeat(False)

# ===== LoRaWAN =====

# Initialisation du réseau LoRaWAN en fonction de sa région
# Europe = LoRa.EU868
# LoRaWAN 1.0.2
lora = LoRa(mode=LoRa.LORAWAN, region=LoRa.EU868)

# DevEUI == adresse MAC de notre carte LoPy4
dev_eui = ubinascii.unhexlify(ubinascii.hexlify(lora.mac()).decode('ascii'))
print("DevEUI: %s" % (ubinascii.hexlify(lora.mac()).decode('ascii')))

# AppEUI non nécéssaire pour CampusIoT
app_eui = ubinascii.unhexlify('0000000000000000')
print("AppEUI: 0000000000000000")

# AppKEY == valeur ASCII de "projet_info-4_20"
app_key = ubinascii.unhexlify('70726F6A65745F696E666F2D345F3230')
print("AppKEY: 70726F6A65745F696E666F2D345F3230")

lora.join(activation=LoRa.OTAA, auth=(dev_eui, app_eui, app_key), timeout=0) # Connexion à CampusIoT

# Attente que l'on joined CampusIoT
# ~2-3 minutes d'attente (dans le meilleur des cas)
print("Connexion", end = '')
failSafe = 0

while not lora.has_joined():
    failSafe += 1
    time.sleep(2.5)
    print('.', end='')
    if failSafe >= 120: # Si on atteint 5 minutes d'attente on renvoie une erreur
        raise Exception("Error while connecting, restart the device.")

pycom.rgbled(color(0, 0, 16))

print("")
print('Joined')

# ===== Socket LoRa =====

socket_data_rate = 5

# Création d'une socket LoRa
s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

# On met le data rate souhaité (applicable seulement dans notre mode LoRaWAN)
s.setsockopt(socket.SOL_LORA, socket.SO_DR, socket_data_rate)

# Socket non bloquante
s.setblocking(False)

# ===== Bluetooth Low Energy =====

# Initialisation du BLE sur la carte LoPy4.
bluetooth = Bluetooth()
# Création de l'identifiant unique (uuid) et initialisation des paramètres de la diffusion.
bluetooth.set_advertisement(name='LoPy4', service_uuid=uuid2bytes('494E464F-3441-5338-3230-50524F4A4554'))
# L'identifiant unique (uuid) devrait être choisie en s'enregistrant sur des platformes en ligne pour s'assurer que l'identifiant ne va pas impacter d'autre service sur le réseaux BLE.
# Nous avons choisie l'identifiant suivant en convertisant INFO-4A-S8-20-PROJET dans leur code hexadécimal en ASCII.

# Fonction permettant de gérer la connexion et deconnexion des clients
def handle_connection (ble):
    events = ble.events()
    if  events & Bluetooth.CLIENT_CONNECTED:
        pycom.rgbled(color(0, 16, 0))
        print("Client connected")
    elif events & Bluetooth.CLIENT_DISCONNECTED:
        print("Client disconnected")

# Création du callback pour lors de la connexion/deconnexion des clients
bluetooth.callback(trigger=Bluetooth.CLIENT_CONNECTED | Bluetooth.CLIENT_DISCONNECTED, handler=handle_connection)

# Activation de la diffusion sur le réseau BLE (a partir d'ici, des clients peuvent se connecter).
bluetooth.advertise(True)

serv = bluetooth.service(uuid=uuid2bytes('494E464F-3441-5338-3230-50524F4A4554'), isprimary=True)

char = serv.characteristic(uuid=uuid2bytes('494E464F-3441-5338-3230-50524F4A4554'), value=5)

def handle_communication(chr, t):
    pycom.rgbled(color(160, 0, 0))
    events = chr.events()
    if events & Bluetooth.CHAR_WRITE_EVENT:
        print("Reception")
        if chr.value() == '0': #Generation d'un test du réseau
            ns = socket.socket(socket.AF_LORA, socket.SOCK_RAW)
            ns.setsockopt(socket.SOL_LORA, socket.SO_DR, socket_data_rate)
            ns.send('LoRa')
        else: #Reception du résultat du test
            socket_data_rate = chr.value()
    else: #events & Bluetooth.CHAR_READ_EVENT
        print("Envoie")
        #Pas d'envoie necessaire si l'on passe par le serveur LoRa

char.callback(trigger=Bluetooth.CHAR_WRITE_EVENT | Bluetooth.CHAR_READ_EVENT, handler=handle_communication)


wait = 0
while(True):
    s.send('LoRa')

    data = s.recv(64)
    if data != b'':
        print("data = ")
        print(data)
    wait += 1

    time.sleep(10)

