def color_i(red:int, green:int, blue:int) -> int:
    red = red & 0xff
    green = green & 0xff
    blue = blue & 0xff
    return (red << 8*2) + (green << 8) + blue

def color_f(red:float, green:float, blue:float) -> int:
    redi = (int) (red*0xff)
    greeni = (int) (green*0xff)
    bluei = (int) (blue*0xff)
    return (redi << 8*2) + (greeni << 8) + bluei